<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Settings;

class SettingsTableSeeder extends Seeder
{
    private $data = [
        [
            'key'        => 'overtime_method',
            'value'      => 1,
            'expression' => '(salary / 173) * overtime_duration_total'
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $value) {
            Settings::updateOrCreate($value, $value);
        }
    }
}
