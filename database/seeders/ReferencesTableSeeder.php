<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\References;

class ReferencesTableSeeder extends Seeder
{
    private $data = [
        [
            'code' => 'overtime_method',
            'name' => 'Salary / 173',
            'expression' => '(salary / 173) * overtime_duration_total'
        ],
        [
            'code' => 'overtime_method',
            'name' => 'Fixed',
            'expression' => '10000 * overtime_duration_total'
        ],
        [
            'code' => 'employee_status',
            'name' => 'Tetap',
            'expression' => null
        ],
        [
            'code' => 'employee_status',
            'name' => 'Percobaan',
            'expression' => null
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $value) {
            References::updateOrCreate($value, $value);
        }
    }
}
