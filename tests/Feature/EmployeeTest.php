<?php

namespace Tests\Feature;

use Tests\TestCase;

class EmployeeTest extends TestCase
{
    private $path = '/api/employees';

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_index_default()
    {
        $this->default('GET', $this->path, []);
    }

    public function test_index_with_pagination()
    {
        $this->default('GET', $this->path, ['per_page' => 1]);
    }

    public function test_index_with_page()
    {
        $this->default('GET', $this->path, ['page' => 1]);
    }

    public function test_index_with_order_by()
    {
        $this->default('GET', $this->path, ['order_by' => 'name']);
    }

    public function test_index_with_order_type()
    {
        $this->default('GET', $this->path, ['order_type' => 'ASC']);
    }

    public function test_index_with_pagination_page()
    {
        $this->default('GET', $this->path, ['pagination' => 2, 'page' => 1]);
    }

    public function test_index_with_pagination_failed()
    {
        $this->default('GET', $this->path, ['per_page' => 'per_page']);
    }

    public function test_index_with_page_failed()
    {
        $this->default('GET', $this->path, ['page' => 'page']);
    }

    public function test_index_with_order_by_failed()
    {
        $this->default('GET', $this->path, ['order_by' => 'id']);
    }

    public function test_index_with_order_type_failed()
    {
        $this->default('GET', $this->path, ['order_type' => 'asc']);
    }

    public function test_index_with_pagination_page_failed()
    {
        $this->default('GET', $this->path, ['pagination' => 'a', 'page' => 'a']);
    }

    public function test_create()
    {
        $this->default('POST', $this->path, ['name' => 'agus', 'status_id' => 3, 'salary' => 2000000]);
    }

    public function test_create_failed()
    {
        $this->default('POST', $this->path, ['name' => 'a', 'status_id' => 1, 'salary' => 1000000]);
    }
}
