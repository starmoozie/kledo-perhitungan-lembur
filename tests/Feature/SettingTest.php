<?php

namespace Tests\Feature;

use Tests\TestCase;

class SettingTest extends TestCase
{
    private $path = '/api/settings';

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_update()
    {
        $this->default('PATCH', $this->path, ['key' => 'overtime_method', 'value' => 1]);
    }

    public function test_update_failed()
    {
        $this->default('PATCH', $this->path, ['key' => 'overtime', 'value' => 5]);
    }
}
