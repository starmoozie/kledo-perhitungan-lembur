<?php

namespace Tests\Feature;

use Tests\TestCase;

class OvertimeTest extends TestCase
{
    private $path_default   = '/api/overtimes';
    private $path_calculate = '/api/overtime-pays/calculate';

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_create()
    {
        $employee = \App\Models\Employees::first();
        $overtime = \App\Models\Overtimes::orderByDesc('date')->first();

        $this->default('POST', $this->path_default, [
            'employee_id'  => $employee->id,
            'date'         => \Carbon\Carbon::parse($overtime->date)->subDays(1)->format('Y-m-d'),
            'time_started' => '17:00',
            'time_ended'   => '20:00'
        ]);
    }

    public function test_create_failed()
    {
        $this->default('POST', $this->path_default, [
            'employee_id'  => 5,
            'date'         => '2022-06-21',
            'time_started' => '17:00',
            'time_ended'   => '15:00'
        ]);
    }

    public function test_calculate()
    {
        $this->default('GET', $this->path_calculate, [
            'month' => '2022-04',
        ]);
    }

    public function test_calculate_failed()
    {
        $this->default('GET', $this->path_calculate, [
            'month' => '2022-22',
        ]);
    }

    public function test_index()
    {
        $this->default('GET', $this->path_default, [
            'date_started' => '2022-04-19',
            'date_ended'   => '2022-04-25'
        ]);
    }

    public function test_index_failed()
    {
        $this->default('GET', $this->path_default, [
            'date_started' => '2022-04-29',
            'date_ended'   => '2022-04-25'
        ]);
    }
}
