<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function printDie($params, \Throwable $ex, $response)
    {
        $content = substr($response->getContent(), 0, 1500);
        $trace   = debug_backtrace();
        $error   = [
            'class'   => static::class.'::'.$trace[2]['function'],
            'params'  => $params,
            'content' => $content,
        ];

        dump($error);
    }

    public function default($method = 'GET', $path,  $body = [], $code = 200)
    {
        $response = $this->json($method, $path, $body);

        try {
            $response->assertStatus($code);
        } catch (\Throwable $th) {
            $this->printDie($body, $th, $response);
        }
    }
}
