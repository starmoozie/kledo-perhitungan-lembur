## System requirement
1. php 7.4
2. composer

## Install
1. "composer install"
2. "php artisan migrate --seed" ( Untuk seeding references & settings )
3. "php artisan ser"

## Unit Testing
1. "php artisan test"

## Note
1. Untuk perhitungan karyawan tetap, saya menggunakan metode sendiri, dikarenakan tidak ada keterangan perhitungan untuk karyawan tetap.
Cara saya untuk menghitung karyawan tetap adalah sesuai dengan pembulatan jumlah /jam lembur karyawan.

## Swagger
1. Untuk file swagger bertipe xml, sudah saya include pada direktori root