<?php

namespace App\Interfaces;

interface EmployeeInterfaces {
    public function index($request);
    public function store($request);
}