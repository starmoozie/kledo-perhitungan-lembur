<?php

namespace App\Interfaces;

interface OvertimeInterfaces {
    public function index($request);
    public function store($request);
    public function calculate($request);
}