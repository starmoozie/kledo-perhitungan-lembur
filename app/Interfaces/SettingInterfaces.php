<?php

namespace App\Interfaces;

interface SettingInterfaces {
    public function update($data);
}