<?php

namespace App\Repositories;

use App\Interfaces\OvertimeInterfaces;
use App\Models\Employees;
use App\Models\Overtimes;
use Carbon\Carbon;

class OvertimeRepositories implements OvertimeInterfaces
{
    /**
     * Get all overtime data.
     * @param $request Object
     * @return object
     */
    public function index($request)
    {
        return Overtimes::whereBetween(
            'date',
            $request->only([
                'date_started', 'date_ended'
            ])
        )
        ->with(['employee:id,name'])
        ->get();
    }

    /**
     * Store data.
     * @param $request Object
     * @return json response
     */
    public function store($request) {
        return Overtimes::create(
            $request->only([
            'employee_id',
            'date',
            'time_started',
            'time_ended'
            ])
        );
    }

    /**
     * Calculate payment by overtime.
     * @param $request Object
     * @return json response
     */
    public function calculate($request)
    {
        $filter_month = explode('-', $request->month);

        // Filter by date and year
        $condition    = fn($q) => $q->whereMonth('date', end($filter_month))->whereYear('date', reset($filter_month));

        return Employees::with([
            'status:id,name',
            'overtimes' => $condition
        ])
        ->get()
        ->map(function($employee) {
            $employee->overtimes = $employee->overtimes->map(function($overtime) use ($employee) {
                $start_time = Carbon::parse($overtime->time_started);
                $end_time   = Carbon::parse($overtime->time_ended);
                // Pengurangan start_time - end_time
                $overtime_duration = $start_time->diffInHours($end_time);
                $mod               = $overtime_duration % 2;

                $overtime->overtime_duration = $overtime_duration;
                // Handle rumus sendiri, jika percobaan, maka perhitungan berdasarkan modulus, else tetap perhitungan berdasarkan pembulatan jam lembur
                $overtime->mod               = $employee->status_id === 4 ? $mod === 0 && $overtime_duration !== 0 ? 1 : 0 : $overtime_duration;
                return $overtime;
            });
            // Sum total overtimes
            $employee->overtime_duration_total = $employee->overtimes->sum('mod');

            // Get setting
            $setting    = \App\Models\Settings::first();
            $expression = $setting->expression;

            // Replace expression with employee salary & overtimes total
            $rumus = str_replace(
                ['salary', 'overtime_duration_total'],
                [$employee->salary, $employee->overtime_duration_total],
                $expression
            );
            // Count by expression
            $upah_lembur      = eval('return '.$rumus.';');
            // Pembulatan 3 angka dibelakang koma
            $employee->amount = round($upah_lembur, 3);

            return $employee;
        });
    }
}
