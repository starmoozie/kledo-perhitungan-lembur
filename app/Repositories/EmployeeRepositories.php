<?php

namespace App\Repositories;

use App\Interfaces\EmployeeInterfaces;
use App\Models\Employees;

class EmployeeRepositories implements EmployeeInterfaces
{
    /**
     * Get all employee data.
     * @param $request Object
     * @return object
     */
    public function index($request)
    {
        return Employees::with(['status:id,name'])
        ->orderBy(
            $request->order_by ?: 'name',
            $request->order_type ?: 'ASC'
        )
        ->paginate(
            $request->per_page ?: 10,
            ['*'],
            'page',
            $request->page ?: 1
        );
    }

    /**
     * Store data.
     * @param $request Object
     * @return object
     */
    public function store($request) {
        return Employees::create(
            $request->only([
            'name',
            'salary',
            'status_id',
            ])
        );
    }
}
