<?php

namespace App\Repositories;

use App\Interfaces\SettingInterfaces;
use App\Models\Settings;

class SettingRepositories implements SettingInterfaces
{
    /**
     * Update logic.
     * @param $data Object
     * @return boolean
     */
    public function update($data) {

        // Find setting by key
        $entry = Settings::findOrFail($data->key);

        // Update setting entry 
        return $entry->update([
            'key'        => $data->key,
            'value'      => $data->value,
            'expression' => $entry->references->expression // Get expression column from related model
        ]);
    }
}
