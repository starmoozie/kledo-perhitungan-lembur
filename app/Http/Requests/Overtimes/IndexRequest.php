<?php

namespace App\Http\Requests\Overtimes;

use Illuminate\Foundation\Http\FormRequest;

class IndexRequest extends FormRequest
{
    use \App\Traits\ErrorValidation;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date_started' => [
                'required',
                'date_format:Y-m-d',
                'before_or_equal:date_ended'
            ],
            'date_ended'   => [
                'required',
                'date_format:Y-m-d',
                'after_or_equal:time_started'
            ]
        ];
    }
}
