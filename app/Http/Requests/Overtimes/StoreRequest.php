<?php

namespace App\Http\Requests\Overtimes;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Rules\OvertimeDateRule;
use App\Models\Employees;

class StoreRequest extends FormRequest
{
    use \App\Traits\ErrorValidation;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_id'  => [
                'required',
                Rule::exists(Employees::class, 'id')
            ],
            'date'         => [
                'required',
                'date_format:Y-m-d',
                'before_or_equal:'.date('Y-m-d'),
                new OvertimeDateRule(request()->employee_id)
            ],
            'time_started' => [
                'required',
                'date_format:H:i',
                'before:time_ended'
            ],
            'time_ended'   => [
                'required',
                'date_format:H:i',
                'after:time_started'
            ]
        ];
    }
}
