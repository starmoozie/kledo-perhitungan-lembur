<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Rules\ReferenceIdRule;
use App\Models\Employees;

class EmployeeRequest extends FormRequest
{
    use \App\Traits\ErrorValidation;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'name'      => [
                        'required',
                        'string',
                        'min:2',
                        'max:70',
                        Rule::unique(Employees::class)
                    ],
                    'status_id' => [
                        'required',
                        'integer',
                        new ReferenceIdRule
                    ],
                    'salary'    => 'required|integer|numeric|gte:2000000|lte:10000000'
                ];

            default:
                return [
                    'per_page'   => 'nullable|integer',
                    'page'       => 'nullable|integer',
                    'order_by'   => [
                        'nullable',
                        'string',
                        Rule::in(Employees::getSortableColumns())
                    ],
                    'order_type' => [
                        'nullable',
                        Rule::in(['ASC', 'DESC'])
                    ]
                ];
        }
    }
}
