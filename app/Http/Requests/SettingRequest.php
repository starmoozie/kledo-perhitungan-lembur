<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\SettingKeyRule;
use App\Rules\ReferenceIdRule;

class SettingRequest extends FormRequest
{
    use \App\Traits\ErrorValidation;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'key'   => [
                'required',
                new SettingKeyRule
            ],
            'value' => [
                'required',
                new ReferenceIdRule
            ]
        ];
    }
}
