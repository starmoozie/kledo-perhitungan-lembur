<?php

namespace App\Http\Controllers;

use App\Http\Requests\SettingRequest;
use App\Interfaces\SettingInterfaces;

class SettingsController extends Controller
{
    use \App\Traits\ResponseMessage;

    private $repository;

    public function __construct(SettingInterfaces $repository) 
    {
        $this->repository = $repository;
    }

    /**
     * Update data.
     * @param $request Object
     * @return json response
     */
    public function update(SettingRequest $request)
    {
        try {
            return $this->success(null, $this->repository->update($request));
        } catch (\Throwable $th) {
            return $this->failed($th->getMessage());
        }
    }
}
