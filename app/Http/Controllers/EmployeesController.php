<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeRequest;
use App\Interfaces\EmployeeInterfaces;

class EmployeesController extends Controller
{
    use \App\Traits\ResponseMessage;

    private $repository;

    public function __construct(EmployeeInterfaces $repository) 
    {
        $this->repository = $repository;
    }

    /**
     * Get all employee data.
     * @param $request Object
     * @return json response
     */
    public function index(EmployeeRequest $request)
    {
        try {
            return $this->success(
                null,
                $this->repository->index($request)
            );
        } catch (\Throwable $th) {
            return $this->failed($th->getMessage());
        }
    }

    /**
     * Store data.
     * @param $request Object
     * @return json response
     */
    public function store(EmployeeRequest $request)
    {
        try {
            return $this->success(
                null,
                $this->repository->store($request)
            );
        } catch (\Throwable $th) {
            return $this->failed($th->getMessage());
        }
    }
}
