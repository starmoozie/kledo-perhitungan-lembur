<?php

namespace App\Http\Controllers;

use App\Http\Requests\Overtimes\IndexRequest;
use App\Http\Requests\Overtimes\StoreRequest;
use App\Http\Requests\Overtimes\CalculateRequest;
use App\Interfaces\OvertimeInterfaces;

class OvertimesController extends Controller
{
    use \App\Traits\ResponseMessage;

    private $repository;

    public function __construct(OvertimeInterfaces $repository) 
    {
        $this->repository = $repository;
    }

    /**
     * Get all Overtimes.
     * @param $request Object
     * @return json response
     */
    public function index(IndexRequest $request)
    {
        try {
            return $this->success(
                null,
                $this->repository->index($request)
            );
        } catch (\Throwable $th) {
            return $this->failed($th->getMessage());
        }
    }

    /**
     * Store data.
     * @param $request Object
     * @return json response
     */
    public function store(StoreRequest $request)
    {
        try {
            return $this->success(
                null,
                $this->repository->store($request)
            );
        } catch (\Throwable $th) {
            return $this->failed($th->getMessage());
        }
    }

    /**
     * Get all employee with calculate payment overtime.
     * @param $request Object
     * @return json response
     */
    public function calculate(CalculateRequest $request)
    {
        try {
            return $this->success(
                null,
                $this->repository->calculate($request)
            );
        } catch (\Throwable $th) {
            return $this->failed($th->getMessage());
        }
    }
}
