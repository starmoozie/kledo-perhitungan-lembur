<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Overtimes;

class OvertimeDateRule implements Rule
{
    private $employee_id;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($employee_id)
    {
        $this->employee_id = $employee_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $overtimes = Overtimes::filterByDateEmployee($this->employee_id, $value)
        ->get();

        return !$overtimes->isEmpty() ? false : true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.unique');
    }
}
