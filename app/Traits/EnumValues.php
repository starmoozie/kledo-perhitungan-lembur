<?php

namespace App\Traits;

/**
 * 
 */
trait EnumValues
{
    /**
     * Get all enum valus from model.
     * @param $field_name string (column_name)
     * @return array
     */
    public static function getEnumValues($field_name)
    {
        $default_connection = \Config::get('database.default');
        $table_prefix = \Config::get('database.connections.'.$default_connection.'.prefix');

        $instance = new static(); // create an instance of the model to be able to get the table name
        $type = \DB::select(\DB::raw('SHOW COLUMNS FROM `'.$table_prefix.$instance->getTable().'` WHERE Field = "'.$field_name.'"'))[0]->Type;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $enum = [];
        foreach (explode(',', $matches[1]) as $value) {
            $enum[] = trim($value, "'");
        }

        return $enum;
    }
}
