<?php

namespace App\Traits;

trait ResponseMessage
{
    /**
     * Show JSON response.
     * @param $message string
     * @param $data any
     * @param $code integer
     * @return string
     */
    public function success($message, $data, $code = 200)
    {
        return response()
        ->json([
            'success' => true,
            'message' => $message ?: 'Request Successfully.',
            'data'    => $data
        ], $code);
    }

    /**
     * Show JSON response.
     * @param $message string
     * @param $data array
     * @param $code integer
     * @return string
     */
    public function failed($message, $data = [], $error_code = 400)
    {
        return response()
        ->json([
            'success' => false,
            'message' => $message ?: 'Request Failed.',
            'data'    => $data ?: null
        ], $error_code);
    }
}