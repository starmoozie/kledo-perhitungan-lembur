<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Overtimes extends Model
{
    use HasFactory;

    protected $fillable = [
        'employee_id',
        'date',
        'time_started',
        'time_ended'
    ];

    public $timestamps = false;

    /*
    |--------------------------------------------------------------------------
    | RELATIONSHIPS
    |--------------------------------------------------------------------------
    */

    public function employee()
    {
        return $this->belongsTo(Employees::class, 'employee_id', 'id');
    }

    /**
     * Scope query by employee_id and date.
     * @param $query Model instance
     * @param $employee_id integer
     * @param $date date
     * @return model instance
     */
    public function scopeFilterByDateEmployee($query, $employee_id, $date)
    {
        return $query->whereEmployeeId($employee_id)
        ->whereDate('date', $date);
    }
}
