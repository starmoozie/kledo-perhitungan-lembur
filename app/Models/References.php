<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class References extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'name',
        'expression'
    ];

    public $timestamps = false;

    /*
    |--------------------------------------------------------------------------
    | RELATIONSHIPS
    |--------------------------------------------------------------------------
    */

    public function settings()
    {
        return $this->hasOne(Settings::class, 'value', 'id')->whereKey('overtime_method');
    }

    public function employees()
    {
        return $this->hasMany(Employees::class, 'status_id', 'id');
    }

    /**
     * Scope query get overtime_method.
     *
     * @return Model instance
     */
    public function scopeOvertime($query)
    {
        return $query->whereCode('overtime_method');
    }

    /**
     * Scope query get employee_status.
     *
     * @return Model instance
     */
    public function scopeEmployeeStatus($query)
    {
        return $query->whereCode('employee_status');
    }

    /**
     * Scope query get overtime_method or employee_status by route.
     *
     * @return Model instance
     */
    public function scopeCheckCurrentRouteToGetCode($query)
    {
        return $query->when(
            \Route::currentRouteName() === 'setting.update',
            fn($q) => $q->overtime(),
            fn($q) => $q->employeeStatus()
        );
    }

    /**
     * Filter query if $query->id === code.
     * @param $code integer
     * @return Object
     */
    public function scopeFilterCode($query, $code)
    {
        return $query->get()
        ->filter(fn($q) => $q->id === $code);
    }
}
