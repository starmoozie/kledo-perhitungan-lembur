<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    use HasFactory;
    use \App\Traits\EnumValues;

    protected $primaryKey = 'key';

    protected $fillable = [
        'key',
        'value',
        'expression'
    ];

    // Disable inrementing
    public $incrementing  = false;

    // Disable laravel timestamps
    public $timestamps    = false;

    /*
    |--------------------------------------------------------------------------
    | RELATIONSHIPS
    |--------------------------------------------------------------------------
    */

    public function references()
    {
        return $this->belongsTo(References::class, 'value', 'id');
    }
}
