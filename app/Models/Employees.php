<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'status_id',
        'salary'
    ];

    public $timestamps = false;

    /**
     * Get sortable columns from model.
     *
     * @return array
     */
    public static function getSortableColumns()
    {
        $self = new Self();

        return collect($self->getFillable())
        ->filter(fn($q) => $q !== 'status_id')
        ->values()
        ->toArray();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONSHIPS
    |--------------------------------------------------------------------------
    */

    public function references()
    {
        return $this->belongsTo(References::class, 'status_id', 'id');
    }

    public function overtimes()
    {
        return $this->hasMany(Overtimes::class, 'employee_id', 'id');
    }

    public function status()
    {
        return $this->references();
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSOR
    |--------------------------------------------------------------------------
    */

    /**
     * Acessor salary to rupiah.
     *
     * @return string
     */
    public function getSalaryFormatted()
    {
        return rupiah($this->salary);
    }
}
