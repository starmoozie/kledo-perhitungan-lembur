<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

// Setting inc
use App\Interfaces\SettingInterfaces;
use App\Repositories\SettingRepositories;

// Employee inc
use App\Interfaces\EmployeeInterfaces;
use App\Repositories\EmployeeRepositories;

// Employee inc
use App\Interfaces\OvertimeInterfaces;
use App\Repositories\OvertimeRepositories;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SettingInterfaces::class, SettingRepositories::class);
        $this->app->bind(EmployeeInterfaces::class, EmployeeRepositories::class);
        $this->app->bind(OvertimeInterfaces::class, OvertimeRepositories::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
